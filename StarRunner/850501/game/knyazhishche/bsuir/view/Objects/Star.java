package game.knyazhishche.bsuir.view.Objects;

import javafx.scene.image.ImageView;

public class Star extends GameObject {
    public Star(String ImagePath, int radius){
        super(new ImageView(ImagePath), radius);
    }
}
