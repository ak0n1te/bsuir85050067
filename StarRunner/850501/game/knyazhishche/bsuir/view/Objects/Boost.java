package game.knyazhishche.bsuir.view.Objects;

import javafx.scene.image.ImageView;

public class Boost extends GameObject {
    private int MaxSize;
    private boolean State;
    private int BoostShipValue;

    public int getBoostShipValue() { return BoostShipValue; }
    public void setBoostShipValue(int boostShipValue) { this.BoostShipValue = boostShipValue; }

    public boolean getState(){ return this.State; };
    public int getMaxSize(){ return this.MaxSize; };

    public void setState(boolean State) { this.State = State; };
    public void setMaxSize(int MaxSize) { this.MaxSize = MaxSize; };

    public Boost(String ImagePath, int radius) {
        super(new ImageView(ImagePath), radius);
        this.MaxSize = 3;
        this.State = false;
    }
}
