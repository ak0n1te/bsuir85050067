package game.knyazhishche.bsuir.view.Objects;

import javafx.scene.image.ImageView;

public class Heal extends GameObject {
    public Heal(String ImagePath, int radius){
        super(new ImageView(ImagePath), radius);
    }
}
