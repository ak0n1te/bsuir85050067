package bsuir.first_group.laba_yaer_validator;

public class Year {
    public static String checkForLeaping(int year){
        if((year % 4 == 0 || year % 100 == 0) && year % 400 != 0){
            return "Leap Year";
        }
        else {
            return "not a Leap Year";
        }
    }
}
