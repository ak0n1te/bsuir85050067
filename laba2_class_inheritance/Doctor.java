public class Doctor extends Human{
  private String specialty;
  public void diagnose(Illness illness){
    System.out.printf(illness.getIllness());
    treat();
  }
  public void treat(){
    System.out.printf(" Illness is treated\n");
  }
  Doctor(String name, int date, String specialty){
    super(name, date);
    this.specialty = specialty;
  }
  public String getName(){ return this.name; }
}