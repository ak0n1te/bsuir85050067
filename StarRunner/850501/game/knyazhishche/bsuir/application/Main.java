package game.knyazhishche.bsuir.application;

import game.knyazhishche.bsuir.view.MainView;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        try {
            MainView manager = new MainView();
            primaryStage = manager.getMainStage();
            primaryStage.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
