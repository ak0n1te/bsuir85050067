package game.knyazhishche.bsuir.model;

public enum SHIP {
    BLUE("game/knyazhishche/bsuir/view/resources/shipchooser/playerShip_blue.png", "game/knyazhishche/bsuir/view/resources/shipchooser/playerBlueLife.png", "game/knyazhishche/bsuir/view/resources/shipchooser/heal_blue.png", "game/knyazhishche/bsuir/view/resources/shipchooser/blueX.png", "game/knyazhishche/bsuir/view/resources/shipchooser/blueXX.png"),
    GREEN("game/knyazhishche/bsuir/view/resources/shipchooser/playerShip_green.png", "game/knyazhishche/bsuir/view/resources/shipchooser/playerGreenLife.png", "game/knyazhishche/bsuir/view/resources/shipchooser/heal_green.png", "game/knyazhishche/bsuir/view/resources/shipchooser/greenX.png", "game/knyazhishche/bsuir/view/resources/shipchooser/greenXX.png"),
    ORANGE("game/knyazhishche/bsuir/view/resources/shipchooser/playerShip_orange.png", "game/knyazhishche/bsuir/view/resources/shipchooser/playerOrangeLife.png", "game/knyazhishche/bsuir/view/resources/shipchooser/heal_yellow.png", "game/knyazhishche/bsuir/view/resources/shipchooser/orangeX.png", "game/knyazhishche/bsuir/view/resources/shipchooser/orangeXX.png"),
    RED("game/knyazhishche/bsuir/view/resources/shipchooser/playerShip_red.png", "game/knyazhishche/bsuir/view/resources/shipchooser/playerRedLife.png", "game/knyazhishche/bsuir/view/resources/shipchooser/heal_red.png", "game/knyazhishche/bsuir/view/resources/shipchooser/redX.png", "game/knyazhishche/bsuir/view/resources/shipchooser/redXX.png");

    private String urlLife;
    private String urlShip;
    private String urlShipX;
    private String urlShipXX;
    //private String urlShipXXX;
    private String urlHeal;

    private SHIP(String urlShip, String urlLife, String urlHeal, String urlShipX, String urlShipXX){
        this.urlShip = urlShip;
        this.urlLife = urlLife;
        this.urlHeal = urlHeal;
        this.urlShipX = urlShipX;
        this.urlShipXX = urlShipXX;
        //this.urlShipXXX = urlShipXXX;
    }

    public String getUrlShip(){
        return this.urlShip;
    }

    public String getUrlLife() { return this.urlLife; }

    public String getUrlHeal() { return this.urlHeal; }

    public String getUrlShipX() { return this.urlShipX; }

    public String getUrlShipXX() { return this.urlShipXX; }
}
