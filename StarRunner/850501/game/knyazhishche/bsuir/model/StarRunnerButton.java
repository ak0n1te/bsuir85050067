package game.knyazhishche.bsuir.model;

import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseButton;
import javafx.scene.text.Font;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class StarRunnerButton extends Button {
    private final static String FONT_PATH = "850501/game/knyazhishche/bsuir/model/resources/kenvector_future.ttf";
    private final static String BUTTON_PRESSED = "-fx-background-color: transparent; -fx-background-image: url('/game/knyazhishche/bsuir/model/resources/grey_button_pressed.png')";
    private final static String BUTTON_FREE = "-fx-background-color: transparent; -fx-background-image: url('/game/knyazhishche/bsuir/model/resources/grey_button.png')";

    public StarRunnerButton(String text){
        setText(text);
        setButtonFont();
        setPrefWidth(190);
        setPrefHeight(49);
        setStyle(BUTTON_FREE);
        initializeButtonListeners();
    }

    private void setButtonFont() {
        try{
            setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void setButtonPressed() {
        setStyle(BUTTON_PRESSED);
        setPrefHeight(45);
        setLayoutY(getLayoutY() + 4);
    }

    private void setButtonNotPressed() {
        setStyle(BUTTON_FREE);
        setPrefHeight(49);
        setLayoutY(getLayoutY() - 4);
    }

    private void initializeButtonListeners() {

        setOnMousePressed(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                setButtonPressed();
            }
        });

        setOnMouseReleased(event -> {
            if (event.getButton().equals(MouseButton.PRIMARY)) {
                setButtonNotPressed();
            }
        });

        setOnMouseEntered(event -> setEffect(new DropShadow()));

        setOnMouseExited(event -> setEffect(null));
    }
}
