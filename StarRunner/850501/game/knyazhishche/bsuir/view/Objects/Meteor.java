package game.knyazhishche.bsuir.view.Objects;

import javafx.scene.image.ImageView;

public class Meteor extends GameObject {
    public Meteor(String ImagePath, int radius){
        super(new ImageView(ImagePath), radius);
    }
}
