package bsuir.first_group.laba_yaer_validator;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.text.Text;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        GridPane scene = new GridPane();
        scene.setAlignment(Pos.CENTER);
        scene.setHgap(10);
        scene.setVgap(10);
        Button check = new Button();
        check.setText("check");
        Text days = new Text();
        Text yearStatus = new Text();
        TextField input = new TextField();
        check.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                int inputNum = Integer.parseInt(input.getText());
                yearStatus.setText(Year.checkForLeaping(inputNum));
            }
        });

        scene.add(new Label("Year:"), 0,0);
        scene.add(days, 2,0);
        scene.add(yearStatus, 2, 1);
        scene.add(input, 0,1);
        scene.add(check, 1,1);
        StackPane root = new StackPane();
        root.getChildren().add(scene);
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
