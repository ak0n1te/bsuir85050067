package game.knyazhishche.bsuir.view;

import game.knyazhishche.bsuir.model.*;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MainView {
    private static final int HEIGHT = 786, WIDTH = 1024;
    private static final String dbPath = "jdbc:sqlite:D:/sqlite/StarRunner.db";
    private final static String FONT_PATH = "850501/game/knyazhishche/bsuir/model/resources/kenvector_future.ttf";

    private AnchorPane mainPane;
    private Scene mainScene;
    private Stage mainStage;

    private final static int MENU_BUTTONS_START_X = 100;
    private final static int MENU_BUTTONS_START_Y = 150;

    private StarRunnerSubScene credistsSubScene;
    private StarRunnerSubScene scoreSubScene;
    private StarRunnerSubScene shipChooserSubScene;
    private StarRunnerSubScene SignInSubScene;
    private StarRunnerSubScene SignUpSubScene;

    private StarRunnerSubScene sceneToHide;

    private Connection connection;
    List<StarRunnerButton> menuButtons;

    List<ShipPicker> shipsList;
    private SHIP choosenShip;

    private Label scoreLabel;

    private String password;
    private String username;

    private Statement stmp;
    private boolean LOGIN;

    public MainView() throws FileNotFoundException, SQLException {
        connectSQLite();
        menuButtons = new ArrayList<>();
        mainPane = new AnchorPane();
        mainScene = new Scene(mainPane, WIDTH, HEIGHT);
        mainStage = new Stage();
        mainStage.setScene(mainScene);
        createSubScene();
        createButtons();
        createBackground();
        createLogo();
    }

    private boolean connectSQLite(){
        try{
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection(dbPath);
            System.out.println("Connected");
            stmp  = connection.createStatement();
            return true;
        }
        catch(Exception e){
            System.out.println("Can't connect to db");
            System.out.println(e.getMessage());
            return false;
        }
    }


    public void showSubScene(StarRunnerSubScene subScene){
        if(sceneToHide != null){
            sceneToHide.moveSubScene();
        }
        subScene.moveSubScene();
        sceneToHide = subScene;
    }

    private void createSubScene() throws FileNotFoundException, SQLException {
        credistsSubScene = new StarRunnerSubScene();
        mainPane.getChildren().add(credistsSubScene);

        //helpSubScene = new StarRunnerSubScene();
        //mainPane.getChildren().add(helpSubScene);

        //scoreSubScene = new StarRunnerSubScene();
        //mainPane.getChildren().add(scoreSubScene);

        //SignInSubScene = new StarRunnerSubScene();
        //mainPane.getChildren().add(SignInSubScene);

        //SignUpSubScene = new StarRunnerSubScene();
        //mainPane.getChildren().add(SignUpSubScene);
//        shipShoterSubScene = new StarRunnerSubScene();
//        mainPane.getChildren().add(shipShoterSubScene);
        createSignUpScene();
        createScoresScene();
        createShipChoosenScene();
        createSignInScene();
    }

    public void createShipChoosenScene() throws FileNotFoundException {
        shipChooserSubScene = new StarRunnerSubScene();
        mainPane.getChildren().add(shipChooserSubScene);

        InfoLabel chooseShipLabel = new InfoLabel("CHOOSE YOUR SHIP");
        chooseShipLabel.setLayoutY(25);
        chooseShipLabel.setLayoutX(110);

        Label statusLabel = new Label("");
        statusLabel.setLayoutX(110);
        statusLabel.setLayoutY(250);
        statusLabel.setPrefWidth(380);
        statusLabel.setPrefHeight(38);
        statusLabel.setTextFill(Color.RED);
        statusLabel.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));

        shipChooserSubScene.getPane().getChildren().add(statusLabel);
        shipChooserSubScene.getPane().getChildren().add(chooseShipLabel);
        shipChooserSubScene.getPane().getChildren().add(createShipsToChoouse());
        shipChooserSubScene.getPane().getChildren().add(createButtonToStart(statusLabel));
    }


    public void createSignUpScene() throws FileNotFoundException {
        SignUpSubScene = new StarRunnerSubScene();
        mainPane.getChildren().add(SignUpSubScene);

        InfoLabel SignUpLabel = new InfoLabel("SIGN UP");
        SignUpLabel.setLayoutY(25);
        SignUpLabel.setLayoutX(110);

        Label statusLabel = new Label("");
        statusLabel.setLayoutX(110);
        statusLabel.setLayoutY(75);
        statusLabel.setPrefWidth(380);
        statusLabel.setPrefHeight(38);
        statusLabel.setTextFill(Color.RED);
        statusLabel.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));

        TextField textField = new TextField();
        textField.setLayoutX(110);
        textField.setLayoutY(125);
        textField.setPrefWidth(380);
        textField.setPrefHeight(38);
        textField.setPromptText("UserName");
        textField.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));

        PasswordField passwordField = new PasswordField();
        passwordField.setLayoutX(110);
        passwordField.setLayoutY(175);
        passwordField.setPrefWidth(380);
        passwordField.setPrefHeight(38);
        passwordField.setPromptText("Password");
        passwordField.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));


        PasswordField passwordREField = new PasswordField();
        passwordREField.setLayoutX(110);
        passwordREField.setLayoutY(225);
        passwordREField.setPrefWidth(380);
        passwordREField.setPrefHeight(38);
        passwordREField.setPromptText("re-type Password");
        passwordREField.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));

        StarRunnerButton SignUpButton = new StarRunnerButton("SIGN UP");
        SignUpButton.setLayoutX(350);
        SignUpButton.setLayoutY(300);

        SignUpButton.setOnAction(event ->{
            if(passwordField.getText().isEmpty()){
                statusLabel.setText("Incorrect password");
                statusLabel.setTextFill(Color.RED);
                return;
            }
            if(textField.getText().isEmpty()){
                statusLabel.setText("Incorrect user");
                statusLabel.setTextFill(Color.RED);
                return;
            }
            if(!passwordField.getText().equals(passwordREField.getText())){
                statusLabel.setText("Password not equals");
                LOGIN = false;
                statusLabel.setTextFill(Color.RED);
            }
            else {
                try {
                    addToDatabase(textField.getText(), passwordField.getText());
                    LOGIN = true;
                    statusLabel.setText("Success");
                    statusLabel.setTextFill(Color.GREEN);
                } catch (SQLException throwables) {
                    statusLabel.setText("User already exists");
                    statusLabel.setTextFill(Color.RED);
                    LOGIN = false;
                    throwables.printStackTrace();
                }
            }
        });

        SignUpSubScene.getPane().getChildren().add(statusLabel);
        SignUpSubScene.getPane().getChildren().add(SignUpButton);
        SignUpSubScene.getPane().getChildren().add(passwordREField);
        SignUpSubScene.getPane().getChildren().add(textField);
        SignUpSubScene.getPane().getChildren().add(passwordField);
        SignUpSubScene.getPane().getChildren().add(SignUpLabel);
    }

    private void addToDatabase(String UserName, String Password) throws SQLException {
        this.password = Password;
        this.username = UserName;
        stmp.execute("insert into users (username, password) values ('" + UserName+ "','" + Password + "')");

    }

    public void createSignInScene() throws FileNotFoundException {
        SignInSubScene = new StarRunnerSubScene();
        mainPane.getChildren().add(SignInSubScene);

        Label statusLabel = new Label("");
        statusLabel.setLayoutX(110);
        statusLabel.setLayoutY(75);
        statusLabel.setPrefWidth(380);
        statusLabel.setPrefHeight(38);
        statusLabel.setTextFill(Color.RED);
        statusLabel.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));

        TextField textField = new TextField();
        textField.setLayoutX(110);
        textField.setLayoutY(125);
        textField.setPrefWidth(380);
        textField.setPrefHeight(38);
        textField.setPromptText("UserName");
        textField.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));

        PasswordField passwordField = new PasswordField();
        passwordField.setLayoutX(110);
        passwordField.setLayoutY(175);
        passwordField.setPrefWidth(380);
        passwordField.setPrefHeight(38);
        passwordField.setPromptText("Password");
        passwordField.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));

        InfoLabel SignInLabel = new InfoLabel("SIGN IN");
        SignInLabel.setLayoutY(25);
        SignInLabel.setLayoutX(110);

        StarRunnerButton SignInButton = new StarRunnerButton("SIGN IN");
        SignInButton.setLayoutX(350);
        SignInButton.setLayoutY(300);

        SignInButton.setOnAction(event ->{
            try {
                if(!checkDatabase(textField.getText(), passwordField.getText())){
                    LOGIN = false;
                    statusLabel.setText("Wrong Login or Password");
                    statusLabel.setTextFill(Color.RED);
                }
                else {
                    LOGIN = true;
                    username = textField.getText();
                    password = passwordField.getText();
                    statusLabel.setText("Success");
                    statusLabel.setTextFill(Color.GREEN);
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        });

        SignInSubScene.getPane().getChildren().add(SignInButton);
        SignInSubScene.getPane().getChildren().add(SignInLabel);
        SignInSubScene.getPane().getChildren().add(statusLabel);
        SignInSubScene.getPane().getChildren().add(textField);
        SignInSubScene.getPane().getChildren().add(passwordField);
    }


    private Label createInfoLabel(String text,int x,int y) throws FileNotFoundException {
        Label infoLabel = new Label(text);
        infoLabel.setAlignment(Pos.CENTER_LEFT);
        infoLabel.setPrefWidth(188);
        infoLabel.setLayoutX(x);
        infoLabel.setLayoutY(y);
        infoLabel.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));
        return infoLabel;
    }

    private void createScoresScene() throws SQLException, FileNotFoundException {
        scoreSubScene = new StarRunnerSubScene();
        mainPane.getChildren().add(scoreSubScene);

        int counter = 0;
        InfoLabel scoreLabel = new InfoLabel("SCORE");
        scoreLabel.setLayoutY(25);
        scoreLabel.setLayoutX(110);

        Label score = new Label();
        String sqlRequest = "SELECT username, score FROM users ORDER BY score DESC";
        ResultSet rs = stmp.executeQuery(sqlRequest);
        while(rs.next()){
            score = createInfoLabel(Integer.toString(counter+1),150,100 + counter * 32);
            scoreSubScene.getPane().getChildren().add(score);

            score = createInfoLabel(rs.getString("username"),190,100 + counter * 32);
            scoreSubScene.getPane().getChildren().add(score);

            score = createInfoLabel(Integer.toString(rs.getInt("score")),450,100 + counter * 32);
            scoreSubScene.getPane().getChildren().add(score);

            counter++;
            if(counter == 8)
                break;
        }

        this.scoreLabel = score;
        scoreSubScene.getPane().getChildren().add(scoreLabel);
    }

    private HBox createShipsToChoouse(){
        HBox box = new HBox();
        box.setSpacing(20);

        shipsList = new ArrayList<>();
        for(SHIP ship : SHIP.values()){
            ShipPicker shipToPick = new ShipPicker(ship);
            shipsList.add(shipToPick);
            box.getChildren().add(shipToPick);
            shipToPick.setOnMouseClicked(event -> {
                for(ShipPicker shipOne : shipsList){
                    shipOne.setIsCircleChoosen(false);
                }
                shipToPick.setIsCircleChoosen(true);
                choosenShip = shipToPick.getShip();
            });
        }
        box.setLayoutX(300 - (118 * 2));
        box.setLayoutY(100);
        return box;
    }

    public StarRunnerButton createButtonToStart(Label status){
        StarRunnerButton startButton = new StarRunnerButton("START");
        startButton.setLayoutX(350);
        startButton.setLayoutY(300);

        startButton.setOnAction(event ->{
            if(choosenShip != null && LOGIN == true){
                GameView gameManager = new GameView();
                gameManager.createNewGame(mainStage, scoreSubScene, stmp, choosenShip, username, password);
            }
            if(LOGIN != true){
                status.setText("Sign In first");
                status.setTextFill(Color.RED);
            }
            if(choosenShip == null){
                status.setText("Ship is not Choosen");
                status.setTextFill(Color.RED);
            }
        });

        return startButton;
    }

    private boolean checkDatabase(String username, String password) throws SQLException {
        ResultSet st = stmp.executeQuery("select * from users where password = '" + password + "'and username = '" + username + "';");
        if(st.next()){
            return true;
        }
        else
            return false;
    }

    public Stage getMainStage(){
        return mainStage;
    }

    public void addMenuButton(StarRunnerButton button){
        button.setLayoutX(MENU_BUTTONS_START_X);
        button.setLayoutY(MENU_BUTTONS_START_Y + menuButtons.size() * 100);
        menuButtons.add(button);
        mainPane.getChildren().add(button);
    }


    private void createButtons() {
        createStartButton();
        createScoresButton();
        createSignInButton();
        createSignUpButton();
        createExitButton();
    }

    private void createStartButton(){
        StarRunnerButton startButton = new StarRunnerButton("PLAY");
        addMenuButton(startButton);

        startButton.setOnAction(event -> showSubScene(shipChooserSubScene));
    }

    private void createScoresButton(){
        StarRunnerButton scoresButton = new StarRunnerButton("SCORES");
        addMenuButton(scoresButton);

        scoresButton.setOnAction(event -> showSubScene(scoreSubScene));
    }

    private void createSignInButton() {
        StarRunnerButton SignInButton = new StarRunnerButton("SIGNIN");
        addMenuButton(SignInButton);

        SignInButton.setOnAction(event -> showSubScene(SignInSubScene));
    }

    private void createSignUpButton() {
        StarRunnerButton SignUpButton = new StarRunnerButton("SIGNUP");
        addMenuButton(SignUpButton);

        SignUpButton.setOnAction(event -> showSubScene(SignUpSubScene));
    }

    private void createExitButton() {
        StarRunnerButton exitButton = new StarRunnerButton("EXIT");
        addMenuButton(exitButton);

        exitButton.setOnAction(event -> mainStage.close());
    }


    private void createBackground(){
        Image backgroundImage = new Image("game/knyazhishche/bsuir/view/resources/background.png", 256, 256, false, true);
        BackgroundImage background = new BackgroundImage(backgroundImage, BackgroundRepeat.REPEAT, BackgroundRepeat.REPEAT, BackgroundPosition.DEFAULT, BackgroundSize.DEFAULT); // new BackgroundSize(BackgroundSize.AUTO, BackgroundSize.AUTO, false, false, true, true)
        mainPane.setBackground(new Background(background));
    }

    private void createLogo() {
        ImageView logo = new ImageView("game/knyazhishche/bsuir/view/resources/STAR RUNNER.png");
        logo.setLayoutX(453);
        logo.setLayoutY(60);

        logo.setOnMouseEntered(event -> logo.setEffect(new DropShadow(50, Color.BLUEVIOLET)));
        logo.setOnMouseExited(event -> logo.setEffect(null));
        mainPane.getChildren().add(logo);
    }
}