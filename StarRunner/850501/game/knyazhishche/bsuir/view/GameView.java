package game.knyazhishche.bsuir.view;

import game.knyazhishche.bsuir.view.Objects.Boost;
import game.knyazhishche.bsuir.view.Objects.Heal;
import game.knyazhishche.bsuir.model.InfoLabel;
import game.knyazhishche.bsuir.model.StarRunnerSubScene;
import game.knyazhishche.bsuir.view.Objects.Meteor;
import game.knyazhishche.bsuir.view.Objects.Star;
import javafx.animation.AnimationTimer;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import game.knyazhishche.bsuir.model.SHIP;
import game.knyazhishche.bsuir.model.SmallInfoLabel;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class GameView {
    private final static String BACKGROUND_IMAGE = "game/knyazhishche/bsuir/view/resources/backgroundGame.png";
    private static final String METEOR_BROWN = "game/knyazhishche/bsuir/view/resources/shipchooser/meteorBrown.png";
    private static final String METEOR_GREY = "game/knyazhishche/bsuir/view/resources/shipchooser/meteorGrey.png";
    private static final String METEOR_GREY_BIG = "game/knyazhishche/bsuir/view/resources/shipchooser/meteorGreyBig.png";
    private final static String BOOST_UP_SHIP = "game/knyazhishche/bsuir/view/resources/boost.png";
    private final static String GOLD_STAR = "game/knyazhishche/bsuir/view/resources/star_gold.png";
    private static final String FONT_PATH = "850501/game/knyazhishche/bsuir/model/resources/kenvector_future.ttf";
    private final static int SHIP_RADIUS = 32;

    private long startTime = 0;
    private AnchorPane gamePane;
    private Scene gameScene;
    private Stage gameStage;

    private static final int GAME_WIDTH = 600, GAME_HEIGHT = 800;

    private Stage menuStage;
    private ImageView ship;

    private boolean isLeftPressed;
    private boolean isRightPresseed;
    private int angle;
    private AnimationTimer gameTimer;

    private GridPane gridPane1;
    private GridPane gridPane2;

    private int Level = 0;
    //private ImageView[] brownMeteors =  new ImageView[Level + 6];
    //private ImageView[] greyMeteors = new ImageView[Level + 6];
    //private ImageView[] BigMeteors = new ImageView[3];
    private Meteor[] brownMeteors = new Meteor[Level + 6];
    private Meteor[] greyMeteors = new Meteor[Level + 7];
    private Meteor[] BigMeteors = new Meteor[3];

    Random randomPositionGenerator;

    private Heal heal;
    private Star star;

    private Boost boost;
    //private int BOOST_SHIP = 0;
    //private final static int BOOST_RADIUS = 12;
    private int boost_count = 0;

    private Label statusLabel;

    private String username, password;

    private Statement stmp;

    private StarRunnerSubScene mainPane;

    private SmallInfoLabel pointsLabel;
    private ImageView[] playerLifes;
    private int playerLife;
    private int points = 0;


    public GameView() {
        initStage();
        keyListeners();
        randomPositionGenerator = new Random();
    }

    private void keyListeners(){
        gameScene.setOnKeyPressed(event ->  {
            if(event.getCode() == KeyCode.A || event.getCode() == KeyCode.LEFT){
                isLeftPressed = true;
            }
            else if (event.getCode() == KeyCode.D || event.getCode() == KeyCode.RIGHT) {
                isRightPresseed = true;
            }
        });
        gameScene.setOnKeyReleased(event ->  {
            if(event.getCode() == KeyCode.A || event.getCode() == KeyCode.LEFT){
                isLeftPressed = false;
            }
            else if (event.getCode() == KeyCode.D || event.getCode() == KeyCode.RIGHT){
                isRightPresseed = false;
            }
        });
    }

    private void initStage(){
        gamePane = new AnchorPane();
        gameScene = new Scene(gamePane, GAME_WIDTH, GAME_HEIGHT);
        gameStage = new Stage();
        gameStage.setScene(gameScene);
        //gameStage.show();
    }

    public void createNewGame(Stage menuStage, StarRunnerSubScene anchorPane,Statement stmp, SHIP choosenShip, String username, String password){
        this.mainPane = anchorPane;
        this.username = username;
        this.stmp = stmp;
        this.password = password;
        this.menuStage = menuStage;
        this.menuStage.hide();
        createBackground();
        addShip(choosenShip);
        createGameElements(choosenShip);
        startGameLoop(choosenShip);
        this.gameStage.show();
    }

    public void createGameElements(SHIP choosenShip){
        playerLife = 2;
        pointsLabel = new SmallInfoLabel("POINTS: 00");
        pointsLabel.setLayoutX(460);
        pointsLabel.setLayoutY(20);
        gamePane.getChildren().add(pointsLabel);


        statusLabel = new Label();
        statusLabel.setLayoutX(300);
        statusLabel.setLayoutY(80);
        statusLabel.setTextFill(Color.RED);
        statusLabel.toFront();
        try {
            statusLabel.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        gamePane.getChildren().add(statusLabel);

        star = new Star(GOLD_STAR, 12);
        setNewElementPosition(star.getImageView());
        gamePane.getChildren().add(star.getImageView());

        heal = new Heal(choosenShip.getUrlHeal(), 12);
        setNewElementPosition(heal.getImageView());
        gamePane.getChildren().add(heal.getImageView());


        boost = new Boost(BOOST_UP_SHIP, 12);
        setNewElementPosition(boost.getImageView());
        gamePane.getChildren().add(boost.getImageView());

        playerLifes = new ImageView[3];
        for(int i = 0; i < playerLifes.length; ++i){
            playerLifes[i] = new ImageView(choosenShip.getUrlLife());
            playerLifes[i].setLayoutX(455 + (i * 50));
            playerLifes[i].setLayoutY(80);
            gamePane.getChildren().add(playerLifes[i]);
        }

        for(int i = 0; i < brownMeteors.length; ++i) {
            brownMeteors[i] = new Meteor(METEOR_BROWN, 20);
            setNewElementPosition(brownMeteors[i].getImageView());
            gamePane.getChildren().add(brownMeteors[i].getImageView());
        }

        for(int i = 0; i < greyMeteors.length; ++i) {
            greyMeteors[i] = new Meteor(METEOR_GREY, 20);
            setNewElementPosition(greyMeteors[i].getImageView());
            gamePane.getChildren().add(greyMeteors[i].getImageView());
        }

        for(int i = 0; i < BigMeteors.length; ++i){
            BigMeteors[i] = new Meteor(METEOR_GREY_BIG, 55);
            setNewElementPosition(BigMeteors[i].getImageView());
            gamePane.getChildren().add(BigMeteors[i].getImageView());
        }
    }

    private void moveGameElements(long now) {
        if (startTime == 0) { startTime = now / 100000000L; }
        //long Timer = (now / 10000000L) - startTime;
        if (points != 0 && points % 10 == 0){
            //System.out.println(Timer);
            boost.setState(true);
        }

        star.getImageView().setLayoutY(star.getImageView().getLayoutY() + 6);
        star.getImageView().setRotate(star.getImageView().getRotate() - 2);
        heal.getImageView().setLayoutY(heal.getImageView().getLayoutY() + 6);
        heal.getImageView().setRotate(heal.getImageView().getRotate() + 4);
        //System.out.println((now / 100000000L) - startTime);
        if(boost.getState() == true){
            boost.getImageView().setLayoutY(boost.getImageView().getLayoutY() + 7);
            boost.getImageView().setRotate(boost.getImageView().getRotate() - 3);
        }

        for(int i = 0; i < brownMeteors.length; ++i){
            brownMeteors[i].getImageView().setLayoutY(brownMeteors[i].getImageView().getLayoutY() + 6 + Level);
            brownMeteors[i].getImageView().setRotate(brownMeteors[i].getImageView().getRotate() + 4);
        }

        for(int i = 0; i < greyMeteors.length; ++i){
            greyMeteors[i].getImageView().setLayoutY(greyMeteors[i].getImageView().getLayoutY() + 4 + Level);
            greyMeteors[i].getImageView().setRotate(greyMeteors[i].getImageView().getRotate() + 4);
        }

        if(Level >= 1) {
            for (int i = 0; i < BigMeteors.length; ++i) {
                BigMeteors[i].getImageView().setLayoutY(BigMeteors[i].getImageView().getLayoutY() + Level + 2);
                BigMeteors[i].getImageView().setRotate(BigMeteors[i].getImageView().getRotate() + 3);
            }

            ++boost_count;
        }
    }

    private void checkIfElementsAreBehindTheShipAndRelocate(){
        if(star.getImageView().getLayoutY() > 1200){
            setNewElementPosition(star.getImageView());
        }

        if(heal.getImageView().getLayoutY() > 1200){
            setNewElementPosition(heal.getImageView());
        }

        if(boost.getImageView().getLayoutY() > 1200){
            setNewElementPosition(boost.getImageView());
            boost.setState(false);
        }

        for(int i = 0;i < brownMeteors.length; ++i){
            if(brownMeteors[i].getImageView().getLayoutY() > 900){
                setNewElementPosition(brownMeteors[i].getImageView());
            }
        }
        for(int i = 0;i < greyMeteors.length; ++i){
            if(greyMeteors[i].getImageView().getLayoutY() > 900){
                setNewElementPosition(greyMeteors[i].getImageView());
            }
        }
        for(int i = 0;i < BigMeteors.length; ++i){
            if(BigMeteors[i].getImageView().getLayoutY() > 900){
                setNewElementPosition(BigMeteors[i].getImageView());
            }
        }

    }

    private void setNewElementPosition(ImageView image){
        image.setLayoutX(randomPositionGenerator.nextInt(500));
        image.setLayoutY(-(randomPositionGenerator.nextInt(3200) + 600));

    }

    public void addShip(SHIP choosenShip){
        ship = new ImageView(choosenShip.getUrlShip());
        ship.setLayoutX(GAME_WIDTH / 2);
        ship.setLayoutY(GAME_HEIGHT - 90);
        gamePane.getChildren().add(ship);
    }

    public void startGameLoop(SHIP choosenShip){
        gameTimer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                //System.out.println(now);
                moveBackground();
                moveGameElements(now);
                checkIfElementsAreBehindTheShipAndRelocate();
                try {
                    collision(choosenShip);
                } catch (SQLException | FileNotFoundException throwables) {
                    throwables.printStackTrace();
                }
                shipAnimation();
            }
        };

        gameTimer.start();
    }

    private void increaseLevel(){
        if(points >= 1 && points % 10 == 0){
            Level += 1;
        }
    }

    private void shipAnimation(){
        if(isLeftPressed && !isRightPresseed){
            if(angle > -30){
                angle -= 5;
            }
            ship.setRotate(angle);
            if(ship.getLayoutX() > 0){
                ship.setLayoutX(ship.getLayoutX() - 3 - boost.getBoostShipValue());
            }
        }
        if(isRightPresseed && !isLeftPressed){
            if(angle < 30){
                angle += 5;
            }
            ship.setRotate(angle);
            if(ship.getLayoutX() < 500){
                ship.setLayoutX(ship.getLayoutX() + 3 + boost.getBoostShipValue());
            }
        }
        if((!isRightPresseed && !isLeftPressed) || (isLeftPressed && isRightPresseed)){
            if(angle < 0){
                angle += 5;
            }
            else if(angle > 0){
                angle -= 5;
            }
            ship.setRotate(angle);
        }
    }

    private void createBackground(){
        gridPane1 = new GridPane();
        gridPane2 = new GridPane();

        for(int i = 0; i < 12; ++i){
            ImageView backgroundImage1 = new ImageView(BACKGROUND_IMAGE);
            ImageView backgroundImage2 = new ImageView(BACKGROUND_IMAGE);
            GridPane.setConstraints(backgroundImage1, i%3, i / 3);
            GridPane.setConstraints(backgroundImage2, i%3, i / 3);
            gridPane1.getChildren().add(backgroundImage1);
            gridPane2.getChildren().add(backgroundImage2);
        }


        gridPane2.setLayoutY(-1024);
        //gridPane2.setLayoutX(800);

        gamePane.getChildren().addAll(gridPane1, gridPane2);

    }

    private void moveBackground(){
        gridPane1.setLayoutY(gridPane1.getLayoutY() + 1);
        gridPane2.setLayoutY(gridPane2.getLayoutY() + 1);

        if(gridPane1.getLayoutY() >= 1024){
            gridPane1.setLayoutY(-1024);
        }
        if(gridPane2.getLayoutY() >= 1024){
            gridPane2.setLayoutY(-1024);
        }
    }

    private Label createInfoLabel(String text,int x,int y) throws FileNotFoundException {
        Label infoLabel = new Label(text);
        infoLabel.setAlignment(Pos.CENTER_LEFT);
        infoLabel.setPrefWidth(188);
        infoLabel.setLayoutX(x);
        infoLabel.setLayoutY(y);
        infoLabel.setFont(Font.loadFont(new FileInputStream(FONT_PATH), 23));
        return infoLabel;
    }

    private void collision(SHIP choosenShip) throws SQLException, FileNotFoundException {
        if(SHIP_RADIUS + star.getRadius() > calculateDistance(ship.getLayoutX() + 49, star.getImageView().getLayoutX() + 15, ship.getLayoutY() + 37, star.getImageView().getLayoutY() + 15)){
            setNewElementPosition(star.getImageView());

            points++;
            increaseLevel();
            String textToSet = "POINTS : ";
            if( points < 10){
                textToSet = textToSet + "0";
            }
            pointsLabel.setText(textToSet + points);
        }

        if(SHIP_RADIUS + boost.getRadius() > calculateDistance(ship.getLayoutX() + 49, boost.getImageView().getLayoutX() + 15, ship.getLayoutY() + 37, boost.getImageView().getLayoutY() + 15)){
            setNewElementPosition(boost.getImageView());
            updateShip(choosenShip);
            System.out.println(boost.getMaxSize());
            if(boost.getMaxSize() != 1) {
                boost.setBoostShipValue(boost.getBoostShipValue() + 4);
                boost.setMaxSize(boost.getMaxSize() - 1);
            }
            points += 3;
            increaseLevel();
            String textToSet = "POINTS : ";
            if( points < 10){
                textToSet = textToSet + "0";
            }
            pointsLabel.setText(textToSet + points);
        }

        if(SHIP_RADIUS + heal.getRadius() > calculateDistance(ship.getLayoutX() + 49, heal.getImageView().getLayoutX() + 15, ship.getLayoutY() + 37, heal.getImageView().getLayoutY() + 15)){
            addLife(choosenShip);
            setNewElementPosition(heal.getImageView());
        }

        for(int i = 0; i < brownMeteors.length; ++i){
            if(brownMeteors[i].getRadius() + SHIP_RADIUS > calculateDistance(ship.getLayoutX() + 49, brownMeteors[i].getImageView().getLayoutX() + 20, ship.getLayoutY() + 37, brownMeteors[i].getImageView().getLayoutY() + 20)) {
                removeLife();
                setNewElementPosition(brownMeteors[i].getImageView());
            }
        }

        for(int i = 0; i < greyMeteors.length; ++i){
            if(greyMeteors[i].getRadius() + SHIP_RADIUS > calculateDistance(ship.getLayoutX() + 49, greyMeteors[i].getImageView().getLayoutX() + 20, ship.getLayoutY() + 37, greyMeteors[i].getImageView().getLayoutY() + 20)){
                removeLife();
                setNewElementPosition(greyMeteors[i].getImageView());
            }
        }

        for(int i = 0; i < BigMeteors.length; ++i){
            if(BigMeteors[i].getRadius() + SHIP_RADIUS > calculateDistance(ship.getLayoutX() + 49, BigMeteors[i].getImageView().getLayoutX() + 20, ship.getLayoutY() + 37, BigMeteors[i].getImageView().getLayoutY() + 20)){
                GameOver();
                setNewElementPosition(BigMeteors[i].getImageView());
            }
        }


    }

    private void updateShip(SHIP choosenShip){
        if(boost.getBoostShipValue() == 0){
            //ship = new ImageView(choosenShip.getUrlShipX());
            ship.setImage(new Image(choosenShip.getUrlShipX()));
        }
        if(boost.getBoostShipValue() == 4) {
            ship.setImage(new Image(choosenShip.getUrlShipXX()));
            //ship = new ImageView(choosenShip.getUrlShipXX());
        }
    }

    private void addLife(SHIP choosenShip) {
        if (playerLife < 2){
            ++playerLife;
            playerLifes[playerLife] = new ImageView(choosenShip.getUrlLife());
            playerLifes[playerLife].setLayoutX(455 + (playerLife * 50));
            playerLifes[playerLife].setLayoutY(80);
            gamePane.getChildren().add(playerLifes[playerLife]);
        }
    }

    private void removeLife() throws SQLException, FileNotFoundException {
        gamePane.getChildren().remove(playerLifes[playerLife]);
        --playerLife;
        if(playerLife < 0){
            GameOver();
        }
    }

    private void GameOver() throws SQLException, FileNotFoundException {
        updateDatabase(username, password);
        updateScoreLabel();
        showGameOver();
        gameStage.close();
        gameTimer.stop();
        menuStage.show();
    }

    private void showGameOver() {
        statusLabel.setText("GAME OVER");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    private void updateScoreLabel() throws SQLException, FileNotFoundException {
        int counter = 0;

        InfoLabel scoreLabel = new InfoLabel("SCORE");
        scoreLabel.setLayoutY(25);
        scoreLabel.setLayoutX(110);

        Label score = new Label();
        String sqlRequest = "SELECT username, score FROM users ORDER BY score DESC";
        ResultSet rs = stmp.executeQuery(sqlRequest);
        mainPane.getPane().getChildren().clear();
        while(rs.next()){
            score = createInfoLabel(Integer.toString(counter+1),150,100 + counter * 32);
            mainPane.getPane().getChildren().add(score);
            score = createInfoLabel(rs.getString("username"),190,100 + counter * 32);
            mainPane.getPane().getChildren().add(score);
            score = createInfoLabel(Integer.toString(rs.getInt("score")),450,100 + counter * 32);
            mainPane.getPane().getChildren().add(score);
            counter++;
            if(counter == 8)
                break;
        }
        mainPane.getPane().getChildren().add(scoreLabel);
    }

    private boolean updateDatabase(String username, String password) throws SQLException {
        ResultSet set = stmp.executeQuery("SELECT * from users where username = '"+ username +"';");
        if(set.getInt("score") < points){
            boolean exp = stmp.execute("update users set score = " + points + " where username = '" + username + "';");
            return exp;
        }
        return true;
    }

    private double calculateDistance(double x1, double x2, double y1, double y2){
        return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
    }
}