package game.knyazhishche.bsuir.view.Objects;

import javafx.scene.image.ImageView;

public class GameObject {
    protected ImageView imageView;
    private int radius;

    GameObject(ImageView image_path, int radius){
        this.imageView = image_path;
        this.radius = radius;
    };

    public ImageView getImageView(){
        return this.imageView;
    };
    public int getRadius(){
      return this.radius;
    };
}
